package com.sventas.backend.model.pojo.reportes;

import java.util.Date;

public class HistoricoVenta {

	private String nombreCliente;
	
	private int idVenta;
	
	private Date fecha;
	
	private float total;
	
	private float saldo;
	
	public HistoricoVenta() {}

	public HistoricoVenta(String nombreCliente, int idVenta, Date fecha, float total, float saldo) {
		super();
		this.nombreCliente = nombreCliente;
		this.idVenta = idVenta;
		this.fecha = fecha;
		this.total = total;
		this.saldo = saldo;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	
}
