package com.sventas.backend.model.pojo;

import java.util.List;

import com.sventas.backend.model.entity.Venta;

public class VentaCompleta {

	private List<ArticuloEnCarrito> articulos;
	
	private Venta venta;
	
	private float total;
	
	private float pagado;
	
	private int idDireccion;
	
	public VentaCompleta() {}

	public VentaCompleta(List<ArticuloEnCarrito> articulos, Venta venta, float total, float pagado, int idDirec) {
		super();
		this.articulos = articulos;
		this.venta = venta;
		this.total = total;
		this.pagado = pagado;
		this.idDireccion = idDirec;
	}

	public List<ArticuloEnCarrito> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<ArticuloEnCarrito> articulos) {
		this.articulos = articulos;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public float getPagado() {
		return pagado;
	}

	public void setPagado(float pagado) {
		this.pagado = pagado;
	}
	
	public int getIdDireccion() {
		return idDireccion;
	}
	
	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

}
