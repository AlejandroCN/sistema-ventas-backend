package com.sventas.backend.model.pojo.reportes;

public class VentaPorMesPorCliente {
	
	private int mes;
	
	private String nombreCliente;
	
	private double totalVendido;
	
	public VentaPorMesPorCliente() {}

	public VentaPorMesPorCliente(int mes, String nombreCliente, double totalVendido) {
		super();
		this.mes = mes;
		this.nombreCliente = nombreCliente;
		this.totalVendido = totalVendido;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public double getTotalVendido() {
		return totalVendido;
	}

	public void setTotalVendido(double totalVendido) {
		this.totalVendido = totalVendido;
	}

}
