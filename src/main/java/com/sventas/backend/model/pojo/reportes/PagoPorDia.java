package com.sventas.backend.model.pojo.reportes;

public class PagoPorDia {

	private int dia;
	
	private double totalPagos;
	
	public PagoPorDia() {}

	public PagoPorDia(int dia, double totalPagos) {
		super();
		this.dia = dia;
		this.totalPagos = totalPagos;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public double getTotalPagos() {
		return totalPagos;
	}

	public void setTotalPagos(double totalPagos) {
		this.totalPagos = totalPagos;
	}
	
}
