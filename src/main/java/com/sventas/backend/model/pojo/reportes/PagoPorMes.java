package com.sventas.backend.model.pojo.reportes;

public class PagoPorMes {

	private int mes;
	
	private double totalPagos;
	
	public PagoPorMes() {}

	public PagoPorMes(int mes, double totalPagos) {
		super();
		this.mes = mes;
		this.totalPagos = totalPagos;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public double getTotalPagos() {
		return totalPagos;
	}

	public void setTotalPagos(double totalPagos) {
		this.totalPagos = totalPagos;
	}
	
}
