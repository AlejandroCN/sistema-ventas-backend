package com.sventas.backend.model.pojo.reportes;

public class VentaPorMes {

	private int mes;
	
	private double totalVendido;
	
	public VentaPorMes() {}

	public VentaPorMes(int mes, double totalVendido) {
		super();
		this.mes = mes;
		this.totalVendido = totalVendido;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public double getTotalVendido() {
		return totalVendido;
	}

	public void setTotalVendido(double totalVendido) {
		this.totalVendido = totalVendido;
	}
	
}
