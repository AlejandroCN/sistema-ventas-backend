package com.sventas.backend.model.pojo.reportes;

public class VentaPorCliente {
	
	private int idCliente;
	
	private String nombreCliente;
	
	private double totalVendido;
	
	public VentaPorCliente() {}

	public VentaPorCliente(int idCliente, String nombreCliente, double totalVendido) {
		super();
		this.idCliente = idCliente;
		this.nombreCliente = nombreCliente;
		this.totalVendido = totalVendido;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public double getTotalVendido() {
		return totalVendido;
	}

	public void setTotalVendido(double totalVendido) {
		this.totalVendido = totalVendido;
	}

}
