package com.sventas.backend.model.pojo.reportes;

public class VentaPorArticulo {
	
	private int idArticulo;
	
	private String nombreArticulo;
	
	private long cantidadVendida;
	
	private double total;
	
	public VentaPorArticulo() {}

	public VentaPorArticulo(int idArticulo, String nombreArticulo, long cantidadVendida, double total) {
		super();
		this.idArticulo = idArticulo;
		this.nombreArticulo = nombreArticulo;
		this.cantidadVendida = cantidadVendida;
		this.total = total;
	}

	public int getIdArticulo() {
		return idArticulo;
	}

	public void setIdArticulo(int idArticulo) {
		this.idArticulo = idArticulo;
	}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}

	public long getCantidadVendida() {
		return cantidadVendida;
	}

	public void setCantidadVendida(long cantidadVendida) {
		this.cantidadVendida = cantidadVendida;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
}
