package com.sventas.backend.model.pojo.reportes;

public class ExistenciaArticulo {
	
	private int id;
	
	private String nombreArticulo;
	
	private int stock;
	
	public ExistenciaArticulo() {}

	public ExistenciaArticulo(int id, String nombreArticulo, int stock) {
		super();
		this.id = id;
		this.nombreArticulo = nombreArticulo;
		this.stock = stock;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
}
