package com.sventas.backend.model.pojo.reportes;

import java.util.Date;

public class VentaFactura {
	
	private String nombreCliente;
	
	private int folioVenta;
	
	private Date fecha;
	
	private float total;
	
	public VentaFactura() {}

	public VentaFactura(String nombreCliente, int folioVenta, Date fecha, float total) {
		super();
		this.nombreCliente = nombreCliente;
		this.folioVenta = folioVenta;
		this.fecha = fecha;
		this.total = total;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public int getFolioVenta() {
		return folioVenta;
	}

	public void setFolioVenta(int folioVenta) {
		this.folioVenta = folioVenta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

}
