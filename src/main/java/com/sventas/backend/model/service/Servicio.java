package com.sventas.backend.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sventas.backend.model.dao.IArticuloDao;
import com.sventas.backend.model.dao.ICategoriaDao;
import com.sventas.backend.model.dao.IClienteDao;
import com.sventas.backend.model.dao.IDireccionClienteDao;
import com.sventas.backend.model.dao.IFormaPagoDao;
import com.sventas.backend.model.dao.IPagoParcialDao;
import com.sventas.backend.model.dao.IReporteDao;
import com.sventas.backend.model.dao.IVendedorDao;
import com.sventas.backend.model.dao.IVentaArticuloDao;
import com.sventas.backend.model.dao.IVentaDao;
import com.sventas.backend.model.dao.IVentaEnvioDao;
import com.sventas.backend.model.entity.Articulo;
import com.sventas.backend.model.entity.Categoria;
import com.sventas.backend.model.entity.Cliente;
import com.sventas.backend.model.entity.DireccionCliente;
import com.sventas.backend.model.entity.FormaPago;
import com.sventas.backend.model.entity.PagoParcial;
import com.sventas.backend.model.entity.Vendedor;
import com.sventas.backend.model.entity.Venta;
import com.sventas.backend.model.entity.VentaArticulo;
import com.sventas.backend.model.entity.VentaEnvio;
import com.sventas.backend.model.pojo.reportes.ExistenciaArticulo;
import com.sventas.backend.model.pojo.reportes.HistoricoVenta;
import com.sventas.backend.model.pojo.reportes.PagoPorDia;
import com.sventas.backend.model.pojo.reportes.PagoPorMes;
import com.sventas.backend.model.pojo.reportes.VentaFactura;
import com.sventas.backend.model.pojo.reportes.VentaPorArticulo;
import com.sventas.backend.model.pojo.reportes.VentaPorCliente;
import com.sventas.backend.model.pojo.reportes.VentaPorMes;
import com.sventas.backend.model.pojo.reportes.VentaPorMesPorCliente;

@Service
public class Servicio implements IServicio {
	
	@Autowired
	private IArticuloDao articuloDao;
	
	@Autowired
	private ICategoriaDao categoriaDao;
	
	@Autowired
	private IClienteDao clienteDao;
	
	@Autowired
	private IVendedorDao vendedorDao;

	@Autowired
	private IFormaPagoDao formaPagoDao;
	
	@Autowired
	private IDireccionClienteDao direccionClienteDao;
	
	@Autowired
	private IVentaDao ventaDao;
	
	@Autowired
	private IVentaArticuloDao ventaArticuloDao;
	
	@Autowired
	private IVentaEnvioDao ventaEnvioDao;
	
	@Autowired
	private IPagoParcialDao pagoParcialDao;
	
	@Autowired
	private IReporteDao reporteDao;
	
	//********************************************** ARTICULOS ***********************************************************
	@Override
	public Articulo saveArticulo(Articulo nuevoArticulo) {
		return this.articuloDao.save(nuevoArticulo);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Articulo> findAllArticulo() {
		return (List<Articulo>) this.articuloDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Articulo> findAllArticulosByIdCategoria(int idCategoria) {
		return this.articuloDao.findAllByIdCategoria(idCategoria);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Articulo> findAllArticulosByIdCategoriaAndTermino(int idCategoria, String termino) {
		return this.articuloDao.findAllByIdCategoriaAndTermino(idCategoria, termino);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Articulo> findAllArticulosByTermino(String termino) {
		return this.articuloDao.findAllByTermino(termino);
	}

	//********************************************** CATEGORIAS **********************************************************
	@Override
	@Transactional(readOnly = true)
	public List<Categoria> findAllCategoria() {
		return (List<Categoria>) this.categoriaDao.findAll();
	}

	// ********************************************* CLIENTES ****************************************************
	@Override
	@Transactional(readOnly = true)
	public Cliente findClienteById(int id) {
		return this.clienteDao.findById(id).orElse(null);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAllClientesByTermino(String termino) {
		return this.clienteDao.findAllByTermino(termino);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAllClientes() {
		return (List<Cliente>) this.clienteDao.findAll();
	}

	// ********************************************* VENDEDORES **************************************************
	@Override
	@Transactional(readOnly = true)
	public List<Vendedor> findAllVendedores() {
		return (List<Vendedor>) this.vendedorDao.findAll();
	}

	// ********************************************* FORMAS_PAGO *************************************************
	@Override
	@Transactional(readOnly = true)
	public List<FormaPago> findAllFormasPago() {
		return (List<FormaPago>) this.formaPagoDao.findAll();
	}

	// ********************************************* DIRECCIONES_CLIENTE *****************************************
	@Override
	public DireccionCliente saveDireccionCliente(DireccionCliente nueva) {
		return this.direccionClienteDao.save(nueva);
	}
	
	@Override
	@Transactional(readOnly = true)
	public DireccionCliente findDireccionClienteById(int idDireccionCliente) {
		return this.direccionClienteDao.findById(idDireccionCliente).orElse(null);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<DireccionCliente> findAllDireccionesClienteByIdCliente(int idCliente) {
		return this.direccionClienteDao.findAllByIdCliente(idCliente);
	}

	// ********************************************* VENTAS ******************************************************
	@Override
	public Venta saveVenta(Venta nuevaVenta) {
		return this.ventaDao.save(nuevaVenta);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Venta> findAllVentasDeudorasByCliente(int idCliente) {
		return this.ventaDao.findAllDeudasByIdCliente(idCliente);
	}

	// ********************************************* VENTA_ARTICULOS *********************************************
	@Override
	public VentaArticulo saveVentaArticulo(VentaArticulo nueva) {
		return this.ventaArticuloDao.save(nueva);
	}

	// ********************************************* VENTAS_ENVIO ************************************************
	@Override
	public VentaEnvio saveVentaEnvio(VentaEnvio nueva) {
		return this.ventaEnvioDao.save(nueva);
	}

	// ********************************************* PAGOS_PARCIALES *********************************************
	@Override
	public PagoParcial savePagoParcial(PagoParcial nuevo) {
		return this.pagoParcialDao.save(nuevo);
	}

	// ********************************************* REPORTES ****************************************************
	@Override
	@Transactional(readOnly = true)
	public List<VentaFactura> obtenerVentasPorFactura() {
		return this.reporteDao.getVentasByFactura();
	}

	@Override
	@Transactional(readOnly = true)
	public List<VentaPorArticulo> obtenerVentasPorArticulo() {
		return this.reporteDao.getVentasByArticulo();
	}

	@Override
	@Transactional(readOnly = true)
	public List<VentaPorCliente> obtenerVentasPorcliente() {
		return this.reporteDao.getVentasByCliente();
	}

	@Override
	@Transactional(readOnly = true)
	public List<VentaPorMes> obtenerVentasPorMes() {
		return this.reporteDao.getVentasPorMes();
	}

	@Override
	@Transactional(readOnly = true)
	public List<VentaPorMesPorCliente> obtenerVentasPorMesPorCliente() {
		return this.reporteDao.getVentasPorMesPorCliente();
	}

	@Override
	@Transactional(readOnly = true)
	public List<ExistenciaArticulo> obtenerExistencias() {
		return this.reporteDao.getExistencias();
	}

	@Override
	@Transactional(readOnly = true)
	public List<HistoricoVenta> obtenerHistoricoVentas() {
		return this.reporteDao.getHistoricoVentas();
	}

	@Override
	@Transactional(readOnly = true)
	public List<PagoPorMes> obtenerPagosPorMes() {
		return this.reporteDao.getPagosPorMes();
	}

	@Override
	@Transactional(readOnly = true)
	public List<PagoPorDia> obtenerPagosPorDia() {
		return this.reporteDao.getPagosPorDia();
	}

}
