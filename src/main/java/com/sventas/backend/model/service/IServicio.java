package com.sventas.backend.model.service;

import java.util.List;

import com.sventas.backend.model.entity.Articulo;
import com.sventas.backend.model.entity.Categoria;
import com.sventas.backend.model.entity.Cliente;
import com.sventas.backend.model.entity.DireccionCliente;
import com.sventas.backend.model.entity.FormaPago;
import com.sventas.backend.model.entity.PagoParcial;
import com.sventas.backend.model.entity.Vendedor;
import com.sventas.backend.model.entity.Venta;
import com.sventas.backend.model.entity.VentaArticulo;
import com.sventas.backend.model.entity.VentaEnvio;
import com.sventas.backend.model.pojo.reportes.ExistenciaArticulo;
import com.sventas.backend.model.pojo.reportes.HistoricoVenta;
import com.sventas.backend.model.pojo.reportes.PagoPorDia;
import com.sventas.backend.model.pojo.reportes.PagoPorMes;
import com.sventas.backend.model.pojo.reportes.VentaFactura;
import com.sventas.backend.model.pojo.reportes.VentaPorArticulo;
import com.sventas.backend.model.pojo.reportes.VentaPorCliente;
import com.sventas.backend.model.pojo.reportes.VentaPorMes;
import com.sventas.backend.model.pojo.reportes.VentaPorMesPorCliente;

public interface IServicio {

	//********************************************** ARTICULOS ***************************************************
	public Articulo saveArticulo(Articulo nuevoArticulo);
	
	public List<Articulo> findAllArticulo();
	
	public List<Articulo> findAllArticulosByIdCategoria(int idCategoria);
	
	public List<Articulo> findAllArticulosByIdCategoriaAndTermino(int idCategoria, String termino);
	
	public List<Articulo> findAllArticulosByTermino(String termino);
	
	//********************************************** CATEGORIAS **************************************************
	public List<Categoria> findAllCategoria();
	
	// ********************************************* CLIENTES ****************************************************
	public Cliente findClienteById(int id);
	
	public List<Cliente> findAllClientesByTermino(String termino);
	
	public List<Cliente> findAllClientes();
	
	// ********************************************* VENDEDORES **************************************************
	public List<Vendedor> findAllVendedores();
	
	// ********************************************* FORMAS_PAGO *************************************************
	public List<FormaPago> findAllFormasPago();
	
	// ********************************************* DIRECCIONES_CLIENTE *****************************************
	public DireccionCliente saveDireccionCliente(DireccionCliente nueva);
	
	public DireccionCliente findDireccionClienteById(int idDireccionCliente);
	
	public List<DireccionCliente> findAllDireccionesClienteByIdCliente(int idCliente);
	
	// ********************************************* VENTAS ******************************************************
	public Venta saveVenta(Venta nuevaVenta);
	
	public List<Venta> findAllVentasDeudorasByCliente(int idCliente);
	
	// ********************************************* VENTA_ARTICULOS *********************************************
	public VentaArticulo saveVentaArticulo(VentaArticulo nueva);
	
	// ********************************************* VENTAS_ENVIO ************************************************
	public VentaEnvio saveVentaEnvio(VentaEnvio nueva);
	
	// ********************************************* PAGOS_PARCIALES *********************************************
	public PagoParcial savePagoParcial(PagoParcial nuevo);
	
	// ********************************************* REPORTES ****************************************************
	public List<VentaFactura> obtenerVentasPorFactura();
	
	public List<VentaPorArticulo> obtenerVentasPorArticulo();
	
	public List<VentaPorCliente> obtenerVentasPorcliente();
	
	public List<VentaPorMes> obtenerVentasPorMes();
	
	public List<VentaPorMesPorCliente> obtenerVentasPorMesPorCliente();
	
	public List<ExistenciaArticulo> obtenerExistencias();
	
	public List<HistoricoVenta> obtenerHistoricoVentas();
	
	public List<PagoPorMes> obtenerPagosPorMes();
	
	public List<PagoPorDia> obtenerPagosPorDia();
	
}
