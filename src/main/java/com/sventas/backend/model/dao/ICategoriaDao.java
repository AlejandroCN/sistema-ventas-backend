package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Categoria;

public interface ICategoriaDao extends CrudRepository<Categoria, Integer> {

}
