package com.sventas.backend.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Cliente;

public interface IClienteDao extends CrudRepository<Cliente, Integer> {

	@Query("select c from Cliente c where c.nombre like ?1%")
	public List<Cliente> findAllByTermino(String termino);
	
}
