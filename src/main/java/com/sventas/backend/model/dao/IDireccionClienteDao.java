package com.sventas.backend.model.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.DireccionCliente;

public interface IDireccionClienteDao extends CrudRepository<DireccionCliente, Integer> {
	
	public List<DireccionCliente> findAllByIdCliente(int idCliente);
	
}
