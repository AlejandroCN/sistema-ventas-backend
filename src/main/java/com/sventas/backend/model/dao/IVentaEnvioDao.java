package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.VentaEnvio;

public interface IVentaEnvioDao extends CrudRepository<VentaEnvio, Integer> {

}
