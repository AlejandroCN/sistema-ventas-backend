package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Vendedor;

public interface IVendedorDao extends CrudRepository<Vendedor, Integer> {

}
