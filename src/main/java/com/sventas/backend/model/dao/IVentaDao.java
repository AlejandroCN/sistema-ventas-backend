package com.sventas.backend.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Venta;

public interface IVentaDao extends CrudRepository<Venta, Integer> {

	@Query("select v from Venta v where v.idCliente=?1 and v.saldo>0")
	public List<Venta> findAllDeudasByIdCliente(int idCliente);
	
}
