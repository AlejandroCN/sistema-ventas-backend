package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.PagoParcial;

public interface IPagoParcialDao extends CrudRepository<PagoParcial, Integer> {

}
