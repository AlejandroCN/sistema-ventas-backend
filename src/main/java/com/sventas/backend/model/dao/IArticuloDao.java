package com.sventas.backend.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Articulo;

public interface IArticuloDao extends CrudRepository<Articulo, Integer> {

	public List<Articulo> findAllByIdCategoria(int idCategoria);
	
	@Query("select a from Articulo a"
			+ " where a.idCategoria=?1 and a.nombre like ?2%")
	public List<Articulo> findAllByIdCategoriaAndTermino(int idCategoria, String termino);
	
	@Query("select a from Articulo a where a.nombre like ?1%") 
	public List<Articulo> findAllByTermino(String termino);
	
}
