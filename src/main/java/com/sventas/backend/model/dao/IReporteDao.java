package com.sventas.backend.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Venta;
import com.sventas.backend.model.pojo.reportes.VentaPorArticulo;
import com.sventas.backend.model.pojo.reportes.VentaPorCliente;
import com.sventas.backend.model.pojo.reportes.VentaPorMes;
import com.sventas.backend.model.pojo.reportes.VentaPorMesPorCliente;
import com.sventas.backend.model.pojo.reportes.ExistenciaArticulo;
import com.sventas.backend.model.pojo.reportes.HistoricoVenta;
import com.sventas.backend.model.pojo.reportes.PagoPorDia;
import com.sventas.backend.model.pojo.reportes.PagoPorMes;
import com.sventas.backend.model.pojo.reportes.VentaFactura;

public interface IReporteDao extends CrudRepository<Venta, Integer> {
	
	@Query("select new com.sventas.backend.model.pojo.reportes.VentaFactura(c.nombre, v.id, v.fecha, v.total)"
			+ " from Cliente c, Venta v"
			+ " where c.id=v.idCliente group by v.id")
	public List<VentaFactura> getVentasByFactura();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.VentaPorArticulo"
			+ "(a.id, a.nombre, sum(va.cantidad), sum(va.cantidad * va.precio))"
			+ " from Articulo a, VentaArticulo va"
			+ " where a.id=va.idArticulo group by a.id")
	public List<VentaPorArticulo> getVentasByArticulo();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.VentaPorCliente"
			+ "(c.id, c.nombre, sum(va.precio*va.cantidad))"
			+ " from Cliente c, VentaArticulo va, Venta v"
			+ " where va.idVenta=v.id and v.idCliente=c.id group by c.id")
	public List<VentaPorCliente> getVentasByCliente();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.VentaPorMes"
			+ "(month(v.fecha), sum(va.precio*va.cantidad))"
			+ " from Venta v, VentaArticulo va where v.id=va.idVenta group by month(v.fecha)")
	public List<VentaPorMes> getVentasPorMes();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.VentaPorMesPorCliente"
			+ "(month(v.fecha), c.nombre, sum(va.precio*va.cantidad))"
			+ " from Venta v, Cliente c, VentaArticulo va"
			+ " where c.id=v.idCliente and v.id=va.idVenta group by month(v.fecha), c.nombre")
	public List<VentaPorMesPorCliente> getVentasPorMesPorCliente();

	@Query("select new com.sventas.backend.model.pojo.reportes.ExistenciaArticulo(a.id, a.nombre, a.stock)"
			+ " from Articulo a")
	public List<ExistenciaArticulo> getExistencias();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.HistoricoVenta"
			+ "(c.nombre, v.id, v.fecha, v.total, v.saldo)"
			+ " from Cliente c, Venta v"
			+ " where c.id=v.idCliente group by v.id")
	public List<HistoricoVenta> getHistoricoVentas();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.PagoPorMes(month(pp.fecha), sum(pp.pago))"
			+ " from PagoParcial pp group by month(pp.fecha)")
	public List<PagoPorMes> getPagosPorMes();
	
	@Query("select new com.sventas.backend.model.pojo.reportes.PagoPorDia(day(pp.fecha), sum(pp.pago))"
			+ " from PagoParcial pp group by day(pp.fecha)")
	public List<PagoPorDia> getPagosPorDia();
	
}
