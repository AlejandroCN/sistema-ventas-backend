package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.CompraArticulo;

public interface ICompraArticuloDao extends CrudRepository<CompraArticulo, Integer> {

}
