package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.Proveedor;

public interface IProveedorDao extends CrudRepository<Proveedor, Integer> {

}
