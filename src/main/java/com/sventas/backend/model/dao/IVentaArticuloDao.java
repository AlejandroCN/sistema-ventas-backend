package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.VentaArticulo;

public interface IVentaArticuloDao extends CrudRepository<VentaArticulo, Integer> {

}
