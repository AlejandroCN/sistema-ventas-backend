package com.sventas.backend.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.sventas.backend.model.entity.FormaPago;

public interface IFormaPagoDao extends CrudRepository<FormaPago, Integer> {

}
