package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.Articulo;
import com.sventas.backend.model.service.IServicio;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("sventas/api/articulos")
public class ArticuloRestController {
	
	@Autowired
	private IServicio servicio;
	
	/**
	 * M&eacute;todo que recibe un objeto Articulo y lo persiste en la base de datos
	 * @param nuevoArticulo Recibe el objeto Articulo en formato json
	 * @return regresa un c&oacute;digo 500 si no es posible persistir el registro, o regresa el objeto guardado en caso contrario.
	 */
	@PostMapping("")
	public ResponseEntity<Articulo> save(@RequestBody Articulo nuevoArticulo) {
		Articulo guardado = this.servicio.saveArticulo(nuevoArticulo);
		
		if(guardado == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(guardado);
	}
	
	/**
	 * M&eacute;todo que busca todos los art&iacute;culos en la base de datos sin importar la categor&iacute;a.
	 * @return regresa c&oacute;digo 400 si no es posible realizar la consulta, en caso contrario devuelve la lista de
	 * art&iacute;culos encontrados.
	 */
	@SuppressWarnings("unchecked")
	@GetMapping("")
	public List<Articulo> findAll() {
		List<Articulo> articulos = this.servicio.findAllArticulo();
		
		if(articulos == null) {
			return (List<Articulo>) ResponseEntity.status(400).build();
		}
		return articulos;
	}
	
	/**
	 * M&eacute;todo que busca todos los articulos que tengan un determinado id de categoria.
	 * @param idCategoria es el id de la categoria que tendran los articulos a buscar
	 * @return regresa c&oacute;digo 400 si no se encuentran registros, o la lista de articulos en caso contrario.
	 */
	@SuppressWarnings("unchecked")
	@GetMapping("/categoria/{idCategoria}")
	public List<Articulo> findAllByIdCategoria(@PathVariable int idCategoria) {
		List<Articulo> articulos = this.servicio.findAllArticulosByIdCategoria(idCategoria);
		
		if(articulos == null) {
			return (List<Articulo>) ResponseEntity.status(400).build();
		}
		return articulos;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{idCategoria}/{termino}")
	public List<Articulo> findAllByCategoriaAndTermino(@PathVariable int idCategoria, @PathVariable String termino) {
		List<Articulo> articulos = this.servicio.findAllArticulosByIdCategoriaAndTermino(idCategoria, termino);
		
		if(articulos == null) {
			return (List<Articulo>) ResponseEntity.status(400).build();
		}
		return articulos;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/termino/{termino}")
	public List<Articulo> findAllByTermino(@PathVariable String termino) {
		List<Articulo> articulos = this.servicio.findAllArticulosByTermino(termino);
		
		if(articulos == null) {
			return (List<Articulo>) ResponseEntity.status(400).build();
		}
		return articulos;
	}

}
