package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.pojo.reportes.ExistenciaArticulo;
import com.sventas.backend.model.pojo.reportes.HistoricoVenta;
import com.sventas.backend.model.pojo.reportes.PagoPorDia;
import com.sventas.backend.model.pojo.reportes.PagoPorMes;
import com.sventas.backend.model.pojo.reportes.VentaFactura;
import com.sventas.backend.model.pojo.reportes.VentaPorArticulo;
import com.sventas.backend.model.pojo.reportes.VentaPorCliente;
import com.sventas.backend.model.pojo.reportes.VentaPorMes;
import com.sventas.backend.model.pojo.reportes.VentaPorMesPorCliente;
import com.sventas.backend.model.service.IServicio;

@RestController
@RequestMapping("sventas/api/reportes")
@CrossOrigin(origins = "http://localhost:4200")
public class ReporteRestController {
	
	@Autowired
	private IServicio servicio;
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ventasFactura")
	public List<VentaFactura> getVentasByFactura() {
		List<VentaFactura> ventasPorFactura = this.servicio.obtenerVentasPorFactura();
		
		if(ventasPorFactura == null) {
			return (List<VentaFactura>) ResponseEntity.status(400).build();
		}
		return ventasPorFactura;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ventasArticulo")
	public List<VentaPorArticulo> getVentasPorArticulo() {
		List<VentaPorArticulo> ventasPorArticulo = this.servicio.obtenerVentasPorArticulo();
		
		if(ventasPorArticulo == null) {
			return (List<VentaPorArticulo>) ResponseEntity.status(400).build();
		}
		return ventasPorArticulo;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ventasCliente")
	public List<VentaPorCliente> getVentasPorCliente() {
		List<VentaPorCliente> ventasPorCliente = this.servicio.obtenerVentasPorcliente();
		
		if(ventasPorCliente == null) {
			return (List<VentaPorCliente>) ResponseEntity.status(400).build();
		}
		return ventasPorCliente;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ventasMes")
	public List<VentaPorMes> getVentasPorMes() {
		List<VentaPorMes> ventas = this.servicio.obtenerVentasPorMes();
		
		if(ventas == null) {
			return (List<VentaPorMes>) ResponseEntity.status(400).build();
		}
		return ventas;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ventasMesCliente")
	public List<VentaPorMesPorCliente> getVentaPorMesPorCliente() {
		List<VentaPorMesPorCliente> ventas = this.servicio.obtenerVentasPorMesPorCliente();
		
		if(ventas == null) {
			return (List<VentaPorMesPorCliente>) ResponseEntity.status(400).build();
		}
		return ventas;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/existencias")
	public List<ExistenciaArticulo> getExistencias() {
		List<ExistenciaArticulo> existencias = this.servicio.obtenerExistencias();
		
		if(existencias == null) {
			return (List<ExistenciaArticulo>) ResponseEntity.status(400).build();
		}
		return existencias;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/historico")
	public List<HistoricoVenta> getHistoricoVentas() {
		List<HistoricoVenta> historico = this.servicio.obtenerHistoricoVentas();
		
		if(historico == null) {
			return (List<HistoricoVenta>) ResponseEntity.status(400).build();
		}
		return historico;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/pagosMes")
	public List<PagoPorMes> getPagosPorMes() {
		List<PagoPorMes> pagos = this.servicio.obtenerPagosPorMes();
		
		if(pagos == null) {
			return (List<PagoPorMes>) ResponseEntity.status(400).build();
		}
		return pagos;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/pagosDia")
	public List<PagoPorDia> getPagosPorDia() {
		List<PagoPorDia> pagos = this.servicio.obtenerPagosPorDia();
		
		if(pagos == null) {
			return (List<PagoPorDia>) ResponseEntity.status(400).build();
		}
		return pagos;
	}

}
