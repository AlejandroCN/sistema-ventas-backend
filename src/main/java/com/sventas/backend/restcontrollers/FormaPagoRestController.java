package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.FormaPago;
import com.sventas.backend.model.service.IServicio;

@RestController
@RequestMapping("sventas/api/formasPago")
@CrossOrigin(origins = "http://localhost:4200")
public class FormaPagoRestController {
	
	@Autowired
	private IServicio servicio;
	
	@SuppressWarnings("unchecked")
	@GetMapping("")
	public List<FormaPago> findAll() {
		List<FormaPago> formasPago = this.servicio.findAllFormasPago();
		
		if(formasPago == null) {
			return (List<FormaPago>) ResponseEntity.status(500).build();
		}
		return formasPago;
	}

}
