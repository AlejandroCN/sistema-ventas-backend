package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.Categoria;
import com.sventas.backend.model.service.IServicio;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("sventas/api/categorias")
public class CategoriaRestController {

	@Autowired
	private IServicio servicio;
	
	@SuppressWarnings("unchecked")
	@GetMapping("")
	public List<Categoria> findAll() {
		List<Categoria> categorias = this.servicio.findAllCategoria();
		
		if(categorias == null) {
			return (List<Categoria>) ResponseEntity.status(400).build();
		}
		return categorias;
	}
	
}
