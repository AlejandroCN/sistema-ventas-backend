package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.Venta;
import com.sventas.backend.model.entity.VentaArticulo;
import com.sventas.backend.model.entity.VentaEnvio;
import com.sventas.backend.model.pojo.ArticuloEnCarrito;
import com.sventas.backend.model.pojo.VentaCompleta;
import com.sventas.backend.model.service.IServicio;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("sventas/api/ventas")
public class VentaRestController {
	
	@Autowired
	private IServicio servicio;
	
	@PostMapping("")
	public ResponseEntity<Venta> save(@RequestBody VentaCompleta ventaCompleta) {
		// Si el pago es menor al total se agrega la diferencia al campo Venta.saldo
		if(ventaCompleta.getPagado() < ventaCompleta.getTotal()) {
			ventaCompleta.getVenta().setSaldo(ventaCompleta.getTotal() - ventaCompleta.getPagado());
		}
		
		// Se guarda la venta en la base de datos
		Venta guardada = this.servicio.saveVenta(ventaCompleta.getVenta());
		if(guardada == null) {
			return ResponseEntity.status(500).build();
		}
		
		// Se relaciona la venta con los articulos vendidos
		List<ArticuloEnCarrito> articulosCarrito = ventaCompleta.getArticulos();
		for(int i=0; i<articulosCarrito.size(); i++) {
			VentaArticulo va = new VentaArticulo();
			va.setIdVenta(guardada.getId());
			va.setIdArticulo(articulosCarrito.get(i).getId());
			va.setPrecio(articulosCarrito.get(i).getPrecio());
			va.setCantidad(articulosCarrito.get(i).getCantidad());
			
			this.servicio.saveVentaArticulo(va);
		}
		
		// Se guarda la direccion de envio si y solo si es el caso
		int idDireccionCliente = ventaCompleta.getIdDireccion();
		if(idDireccionCliente != 0) {
			String direccion = this.servicio.findDireccionClienteById(idDireccionCliente).getDireccion();
			VentaEnvio ve = new VentaEnvio();
			ve.setIdVenta(guardada.getId());
			ve.setDireccionEnvio(direccion);
			
			this.servicio.saveVentaEnvio(ve);
		}
		return ResponseEntity.ok(guardada);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{idCliente}")
	public List<Venta> findAllWithSaldoByCliente(@PathVariable int idCliente) {
		List<Venta> ventas = this.servicio.findAllVentasDeudorasByCliente(idCliente);
		
		if(ventas == null) {
			return (List<Venta>) ResponseEntity.status(400).build();
		}
		return ventas;
	}

}
