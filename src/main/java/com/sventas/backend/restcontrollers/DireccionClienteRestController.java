package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.DireccionCliente;
import com.sventas.backend.model.service.IServicio;

@RestController
@RequestMapping("sventas/api/direccionesCliente")
@CrossOrigin(origins = "http://localhost:4200")
public class DireccionClienteRestController {
	
	@Autowired
	private IServicio servicio;
	
	@PostMapping("")
	public ResponseEntity<DireccionCliente> save(@RequestBody DireccionCliente nueva) {
		DireccionCliente guardada = this.servicio.saveDireccionCliente(nueva);
		
		if(guardada == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(guardada);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{idCliente}")
	public List<DireccionCliente> findAllByCliente(@PathVariable int idCliente) {
		List<DireccionCliente> direcciones = this.servicio.findAllDireccionesClienteByIdCliente(idCliente);
		
		if(direcciones == null) {
			return (List<DireccionCliente>) ResponseEntity.status(400).build();
		}
		return direcciones;
	}

}
