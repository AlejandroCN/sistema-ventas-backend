package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.Vendedor;
import com.sventas.backend.model.service.IServicio;

@RestController
@RequestMapping("sventas/api/vendedores")
@CrossOrigin(origins = "http://localhost:4200")
public class VendedorRestController {
	
	@Autowired
	private IServicio servicio;
	
	@SuppressWarnings("unchecked")
	@GetMapping("")
	public List<Vendedor> findAll() {
		List<Vendedor> vendedores = this.servicio.findAllVendedores();
		
		if(vendedores == null) {
			return (List<Vendedor>) ResponseEntity.status(500).build();
		}
		return vendedores;
	}

}
