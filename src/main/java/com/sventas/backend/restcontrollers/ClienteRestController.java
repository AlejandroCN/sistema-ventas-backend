package com.sventas.backend.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.Cliente;
import com.sventas.backend.model.service.IServicio;

@RestController
@RequestMapping("sventas/api/clientes")
@CrossOrigin(origins = "http://localhost:4200")
public class ClienteRestController {
	
	@Autowired
	private IServicio servicio;
	
	@GetMapping("/cliente/{id}")
	public ResponseEntity<Cliente> findById(@PathVariable int id) {
		Cliente encontrado = this.servicio.findClienteById(id);
		
		if(encontrado == null) {
			return ResponseEntity.status(400).build();
		}
		return ResponseEntity.ok(encontrado);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("")
	public List<Cliente> findAll() {
		List<Cliente> clientes = this.servicio.findAllClientes();
		
		if(clientes == null) {
			return (List<Cliente>) ResponseEntity.status(500).build();
		}
		return clientes;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{termino}")
	public List<Cliente> findAllByTermino(@PathVariable String termino) {
		List<Cliente> clientes = this.servicio.findAllClientesByTermino(termino);
		
		if(clientes == null) {
			return (List<Cliente>) ResponseEntity.status(500).build();
		}
		return clientes;
	}

}
