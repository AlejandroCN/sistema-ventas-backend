package com.sventas.backend.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sventas.backend.model.entity.PagoParcial;
import com.sventas.backend.model.service.IServicio;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
@RequestMapping("sventas/api/pagosParciales")
public class PagoParcialRestController {
	
	@Autowired
	private IServicio servicio;
	
	@PostMapping("")
	public ResponseEntity<PagoParcial> save(@RequestBody PagoParcial nuevoPago) {
		PagoParcial guardado = this.servicio.savePagoParcial(nuevoPago);
		
		if(guardado == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(guardado);
	}

}
